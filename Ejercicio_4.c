//
// Created by Administrador on 20/4/2023.
//
#include <stdio.h>

int main(){
    int cantidad;
    int numero;
    int suma = 0;

    printf("Ingrese la cantidad de numeros a ingresar: ");

    scanf("%d", &cantidad);
    int numeros[cantidad];

    for (int i = 0; i < sizeof(numeros) / sizeof(int); i++) {
        printf("Ingrese el %d numero: ", i+1);
        scanf("%d", &numero);
        numeros[i] = numero;
        suma += numero;
    }

    float promedio = (float) suma / (sizeof(numeros) / sizeof(int));
    printf("El promedio es %f" , promedio);

    return 0;
}