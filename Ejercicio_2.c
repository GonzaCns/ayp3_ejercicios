//
// Created by Administrador on 20/4/2023.
//

#include <stdio.h>

int main(){
    int numeros[] = {43, 82, 315, 101};
    int max=0;

    for (int i = 1; i < sizeof(numeros) / sizeof(int); i++) {
        if (numeros[i] > max){
            max = numeros[i];
        }
    }
    printf("El numero maximo es %d" , max);
    return 0;
}
