//
// Created by Administrador on 20/4/2023.
//
#include <stdio.h>

int main(){
    int numeros[] = {43, 82, 9,315, 101, 13};
    int min=numeros[0];

    for (int i = 1; i < sizeof(numeros) / sizeof(int); i++) {
        if (numeros[i] < min){
            min = numeros[i];
        }
    }
    printf("El numero minimo es %d" , min);

    return 0;
}