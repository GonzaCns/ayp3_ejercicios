//
// Created by Administrador on 22/4/2023.
//
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

typedef struct{
    char nombre[50];
    char apellido[50];
    int edad;
} Persona;

void cambiarNombre(Persona * persona){
    system("cls");
    printf("Ingrese nombre: ");
    scanf("%s", persona->nombre);
    printf("Presione una tecla para continuar..");
    getch();
}

void cambiarApellido(Persona * persona){
    system("cls");
    printf("Ingrese apellido: ");
    scanf("%s", persona->apellido);
    printf("Presione una tecla para continuar..");
    getch();
}

void cambiarEdad(Persona * persona){
    system("cls");
    printf("Ingrese apellido: ");
    scanf("%d", &persona->edad);
    printf("Presione una tecla para continuar..");
    getch();
}

void mostrarPersona(Persona * persona){
    system("cls");
    printf("Nombre: %s\n", persona->nombre);
    printf("Apellido: %s\n", persona->apellido);
    printf("Edad: %d\n", persona->edad);
    printf("Presione una tecla para continuar..");
    getch();
}

int main(){
    int opcion;
    char continuar;
    char nombreNuevo[50], apellidoNuevo[50];
    int edadNueva;
    Persona persona;

    do{
        system("cls");
        printf("**************************\n");
        printf("*   1. Cambiar Nombre    *\n");
        printf("*   2. Cambiar Apellido  *\n");
        printf("*   3. Cambiar Edad      *\n");
        printf("*   4. Mostrar Persona   *\n");
        printf("*   5. Salir             *\n");
        printf("**************************\n");
        printf("Seleccione una opcion: ");
        scanf("%d", &opcion);

        switch(opcion){
            case 1:
                cambiarNombre(&persona);
                break;
            case 2:
                cambiarApellido(&persona);
                break;
            case 3:
                cambiarEdad(&persona);
                break;
            case 4:
                mostrarPersona(&persona);
                break;
            case 5:
                break;
            default:
                system("cls");
                printf("Opcion fuera de rango, elija opciones entre 1 y 4.\nPresione una tecla para regresar al menu.");
                getch();
                break;
        }
    }while(opcion != 5);
    return 0;
}

