//
// Created by Administrador on 21/4/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(){
    int opcion;
    char continuar;
    do{
        system("cls");
        printf("***********************\n");
        printf("*   1. Primer Frase   *\n");
        printf("*   2. Segunda Frase  *\n");
        printf("*   3. Tercer Frase   *\n");
        printf("*   4. Salir          *\n");
        printf("***********************\n");
        printf("Seleccione una opcion: ");
        scanf("%d", &opcion);

        switch(opcion){
            case 1:
                system("cls");
                printf("%cSi le muestras magia a un hombre  pensara que es tecnologia, y si le muestras tecnologia a un hombre antiguo pensara que es magia.%c\nPresione una tecla para regresar al menu.", 34, 34);
                getch();
                break;
            case 2:
                system("cls");
                printf("%cUn desarrollador de software no implementa codigo limpio con un latigo atras.%c\nPresione una tecla para regresar al menu.", 34, 34);
                getch();
                break;
            case 3:
                system("cls");
                printf("%c%cFelicidad? Felicidad es ver una se%cal de wifi sin contrase%ca.%c\nPresione una tecla para regresar al menu.", 34,168,164, 164,34);
                getch();
                break;
            case 4:
                break;
            default:
                system("cls");
                printf("Opcion fuera de rango, elija opciones entre 1 y 4.\nPresione una tecla para regresar al menu.");
                getch();
                break;
        }
    }while(opcion != 4);

    return 0;
}