//
// Created by Administrador on 21/4/2023.
//
#include <stdio.h>

int main(){
    int numero;
    printf("Ingrese el numero: ");
    scanf("%d", &numero);

    if(numero%2 == 0){
        printf("El numero %d es par", numero);

    }else{
        printf("El numero %d es impar", numero);
    }
    return 0;
}