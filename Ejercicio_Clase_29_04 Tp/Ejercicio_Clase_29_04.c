//
// Created by Administrador on 29/4/2023.
//

#include <stdio.h>
#include <malloc.h>

typedef struct structNodo{
    int valor;
    struct  structNodo *proximo;
} Nodo;

void modificarNodo(Nodo *nodo){
    nodo->valor = 2;
}

Nodo* agregarElemento(Nodo *lista, int valor){
    Nodo *nodoNuevo = malloc(sizeof (Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if(lista == NULL){
        lista = nodoNuevo;
    }else{
        Nodo *cursor = lista;
        while (cursor->proximo != NULL){
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }

    return lista;
}

int main(){
    Nodo *lista = NULL;

    lista = agregarElemento(lista, 2);
    lista = agregarElemento(lista, 3);
    lista = agregarElemento(lista, 4);
    printf("%d\n", lista->valor);
    printf("%d\n", lista->proximo->valor);
    printf("%d\n", lista->proximo->proximo->valor);

    return 0;
}

