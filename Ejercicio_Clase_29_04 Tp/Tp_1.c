//
// Created by Administrador on 4/5/2023.
//
#include <stdio.h>
#include <malloc.h>
#include <conio.h>

typedef struct structNodo{
    int valor;
    struct  structNodo *proximo;
} Nodo;

Nodo* crearNodo(int valor) {
    Nodo* nuevoNodo = malloc(sizeof(Nodo));
    nuevoNodo->valor = valor;
    nuevoNodo->proximo = NULL;
    return nuevoNodo;
}

void modificarNodo(Nodo *nodo){
    nodo->valor = 2;
}
void ordenarLista(Nodo **lista){
    Nodo *nodoActual = *lista;
    Nodo *nodoOrdenado = NULL;
    int max = 0;

    while(nodoActual != NULL){
        nodoOrdenado = nodoActual->proximo;
        while(nodoOrdenado != NULL){
            if(nodoActual->valor > nodoOrdenado->valor){
                max = nodoActual->valor;
                nodoActual->valor = nodoOrdenado->valor;
                nodoOrdenado->valor = max;
            }
            nodoOrdenado = nodoOrdenado->proximo;
        }
        nodoActual = nodoActual->proximo;
    }
}

void agregarElemento(Nodo **lista, int valor){
    Nodo *nodoNuevo = crearNodo(valor);

    if(*lista == NULL){
        *lista = nodoNuevo;
    }else{
        Nodo *cursor = *lista;
        while (cursor->proximo != NULL){
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }
}

int countNodes(Nodo *lista) {
    int cantidad = 0;
    Nodo* nodo = lista;
    while (nodo != NULL) {
        cantidad++;
        nodo = nodo->proximo;
    }
    return cantidad;
}

int obtenerElemento(Nodo *lista, int posicion){
    Nodo *cursor = lista;
    int pos = 1;
    while(cursor != NULL){
        if(pos == posicion){
            return cursor->valor;
        }
        pos++;
        cursor = cursor->proximo;
    }
}

void eliminarElemento(Nodo **lista, int posicion){
    Nodo *nodoActual = *lista;
    Nodo *nodoAnterior = NULL;
    int pos = 1;

    while(nodoActual != NULL && pos < posicion){
        nodoAnterior = nodoActual;
        nodoActual = nodoActual->proximo;
        pos++;
    }
    if(nodoActual != NULL){
        if(nodoAnterior == NULL){
            *lista = nodoActual->proximo;
        }else{
            nodoAnterior->proximo = nodoActual->proximo;
        }
        free(nodoActual);
    }
}

void imprimirLista(Nodo *lista){
    Nodo *nodoActual = lista;
    while(nodoActual != NULL){
        printf("%d ", nodoActual->valor);
        nodoActual = nodoActual->proximo;
    }
    printf("\n");
}



int main(){
    Nodo *lista = NULL;
    int valor = 0;

    {
        int opcion;
        char continuar;
        do{
            system("cls");
            printf("************************************\n");
            printf("*   1. Agregar Elemento            *\n");
            printf("*   2. Obtener Largo de la Lista   *\n");
            printf("*   3. Obtener por posicion        *\n");
            printf("*   4. Elminar por posicion        *\n");
            printf("*   5. Imprimir Lista              *\n");
            printf("*   6. Salir                       *\n");
            printf("************************************\n");
            printf("Seleccione una opcion: ");
            scanf("%d", &opcion);

            switch(opcion){
                case 1:
                    system("cls");
                    printf("Ingrese un valor a insertar: ");
                    scanf("%d", &valor);
                    agregarElemento(&lista, valor);
                    ordenarLista(&lista);
                    imprimirLista(lista);
                    printf("presione una tecla para continuar");
                    getch();
                    break;
                case 2:
                    system("cls");
                    printf("El tamanio de la lista es: %d.",countNodes(lista));
                    printf("\npresione una tecla para continuar");
                    getch();
                    break;
                case 3:
                    system("cls");
                    printf("Ingrese la posicion para buscar el valor:");
                    scanf("%d", &valor);
                    if(valor <= countNodes(lista)){
                        printf("El valor es: %d", obtenerElemento(lista,valor));
                    }else{
                        printf("La posicion es mayor al largo de la lista.");
                    }
                    printf("\npresione una tecla para continuar");
                    getch();
                    break;
                case 4:
                    system("cls");
                    imprimirLista(lista);
                    printf("Ingrese la posicion para eliminar:");
                    scanf("%d", &valor);
                    if(valor <= countNodes(lista)){
                        eliminarElemento(&lista, valor);
                        printf("Se ha eliminado correctamente.\n");
                    }else{
                        printf("La posicion es mayor al largo de la lista.");
                    }
                    imprimirLista(lista);
                    printf("\npresione una tecla para continuar");
                    getch();
                    break;

                case 5:
                    system("cls");
                    imprimirLista(lista);
                    printf("\npresione una tecla para continuar");
                    getch();
                    break;

                case 6:
                    break;
                default:
                    system("cls");
                    printf("Opcion fuera de rango, elija opciones entre 1 y 6.\nPresione una tecla para regresar al menu.");
                    getch();
                    break;
            }
        }while(opcion != 6);

        return 0;

    }
}
